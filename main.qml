import QtQuick 2.3
import QtQuick.Controls 1.2
import QtQuick.Layouts 1.1

ApplicationWindow {
    visible: true
    title: qsTr("Personal Financial Responsibility and Brain Damage")

    property real back_end_ratio: 0.36

    property string stopall: "https://i.ytimg.com/vi/RH1ekuvSYzE/hqdefault.jpg"
    property string cosby: "http://www.showbiz411.com/wp-content/uploads/2014/11/151784-bill-cosby-as-cliff-huxtable.jpg"

    function income_percent_available(mortgage, obligations, income) {
        return (mortgage + obligations) / income
    }

    function judgement(mortgage, obligations, income) {
        var percent = income_percent_available(parseFloat(mortgage), parseFloat(obligations), parseFloat(income))
        result.text = "Your ratio: " + percent
        if(percent >= back_end_ratio) {
            fateText.text = "Damn yo get your life right"
            fatePhoto.source = stopall
        }
        else {
            fateText.text = "Congrats you are currently in a financially secure status"
            fatePhoto.source = cosby
        }
    }

    Component.onCompleted: {
        minimumHeight = height
        minimumWidth = width
    }

    DoubleValidator {
        id: inputs
        bottom: 0
        decimals: 2
    }

    ColumnLayout {
        RowLayout {
            ColumnLayout {
                Label { text: "Monthly Mortgage or Rent Payment" }
                Label { text: "Non Mortgage / Rent Monthly Obligations (bills, expenses)" }
                Label { text: "Monlthy paycheck" }
            }
            ColumnLayout {
                TextField {
                    id: mortgage
                    validator: inputs
                    placeholderText:("$ (USD)")
                }
                TextField {
                    id: obligations
                    validator: inputs
                    placeholderText:("$ (USD)")
                }
                TextField {
                    id: income
                    validator: inputs
                    placeholderText:("$ (USD)")
                }
            }
        }
        Button {
            enabled: mortgage.length && obligations.length && income.length
            text: "Calcuate Fate"
            onClicked: judgement(mortgage.text, obligations.text, income.text)
            Layout.fillWidth: true
        }
        RowLayout {
            Label { text: "Back End Ratio: " + back_end_ratio }
            Label { id: result }
        }
        Label {
            id: fateText
            text: "Input your obligations and income and I'll tell you how smart you are!"
        }
        Image { id: fatePhoto }
    }
}
